from math import log, exp, fsum
from collections import defaultdict, Counter
from operator import itemgetter
from heapq import nlargest

from tfidf import get_tf_idf


class NaiveBayesClassifier:
    def __init__(self):
        self.classes = defaultdict(int)
        self.counters = defaultdict(Counter)
        self.pc = {}
        self.pwc = {}
        self.nwords = {}

    def train(self, docs):
        "docs: couples of (class, wordlst)"
        tf_idf = get_tf_idf({i: words for i, (_, words) in enumerate(docs)})
        features_sz = max(len(tf_idf), 2000)
        self.vocab = set(w for w, c in nlargest(features_sz, tf_idf.items(), key=itemgetter(1)))
        
        for c, words in docs:
            counter = Counter(w for w in words if w in self.vocab)
            self.classes[c] += 1
            self.counters[c] += counter

        for c in self.classes.keys():
            self.pc[c] = log(self.classes[c] / sum(self.classes.values()))
            self.nwords[c] = sum(self.counters[c].values())
            for w in self.vocab:
                self.pwc[w,c] = log((self.counters[c][w] + 1) / (self.nwords[c] + len(self.vocab)))

    def classify(self, doc):
        "doc: wordlst"
        ret = None
        counter = Counter(doc)
        for c in self.classes.keys():
            proba = self.pc[c] + \
                    fsum(log((self.counters[c][w] + 1) / (self.nwords[c] + len(doc)))
                         for w in doc)
            if ret is None or proba > ret[1]:
                ret = c, proba
        return ret[0], exp(ret[1])
