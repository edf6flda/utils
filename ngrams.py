#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def get_ngrams(iterable, n):
    padding = (n - 1) * [None]
    lst = padding + list(iterable) + padding
    for i in range(len(lst) - n + 1):
            yield tuple(lst[i: i+n])

get_unigrams = lambda lst: get_ngrams(lst, 1)
get_bigrams  = lambda lst: get_ngrams(lst, 2)
get_trigrams = lambda lst: get_ngrams(lst, 3)
