#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from math import log
from collections import Counter


def get_tf_idf(documents):
    """
    documents: a dict of pairs (doc_id, words)
    """
    freqs = {doc_id: Counter(words) for doc_id, words in documents.items()}
    all_freqs = sum(freqs.values(), Counter())
    ndocs = len(documents)
    nwords = sum(all_freqs.values())
    def tf(w):
        return all_freqs[w] / nwords
    def idf(w):
        return log(ndocs / sum(w in f for f in freqs.values()))
    return {word: tf(word) * idf(word) for word in all_freqs}
