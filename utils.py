#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
from collections import defaultdict, Counter


_h2a_dct = {hc : ac for ac, hc in enumerate('אבגדהוזחטיכלמנסעפצקרשת', 0x10840)}
_h2a_dct['.'] = 0x10857

h2a_dct = _h2a_dct.copy()
h2a_dct['ם'] = 0x1084C
h2a_dct['ן'] = 0x1084D
h2a_dct['ץ'] = 0x10851
h2a_dct['ף'] = 0x10850
h2a_dct['ך'] = 0x1084A


h2a_map = str.maketrans(h2a_dct)

def hebrew2aramaic(s):
    return s.translate(h2a_map)

def rem_hsuffixes(s):
    pos = 'םןץףך'.find(s[-1])
    if pos != -1:
        return s[:-1] + 'מנצפכ'[pos]
    return s
        
        

############################################################################

# adapted from http://docs.python.org/3/library/re.html#writing-a-tokenizer
def make_tokenizer(token_spec):
    tok_regex = '|'.join('(?P<%s>%s)' % pair for pair in token_spec)
    get_token = re.compile(tok_regex).match
    def tokenize(s):
        pos = 0
        mo = get_token(s)
        while mo is not None:
            typ = mo.lastgroup
            if typ != 'SKIP':
                val = mo.group(typ)
                yield val
            pos = mo.end()
            mo = get_token(s, pos)
        if pos != len(s):
            raise RuntimeError('Unexpected character %r' % s[pos])
    return tokenize

heb_tokenizer = make_tokenizer([
        # ('NUMBER',  r'\d+'),
        ('TEXT',    r'''[א-ת"']+'''),
        ('PUNCT',   r'[;:.]'),
        ('SKIP',    r'\s'),     # Skip over spaces and tabs
    ])

aram_tokenizer = make_tokenizer([
        # ('NUMBER',  r'\d+'),
        ('TEXT',    r'[\U00010840-\U00010855]+'),
        ('PUNCT',   r'\U00010857'),
        ('SKIP',    r'\s'),     # Skip over spaces and tabs
    ])

############################################################################


def make_index(docs):
    "docs is couples (docid, wordlst)"
    index = defaultdict(list)
    for docid, wlst in docs:
        for i, word in enumerate(wlst):
            index[word].append((docid, i))
    return index

def concordance(word, index, docs):
    """docs is a dict {docid: wlst}"""
    lastdocid = None
    for docid, pos in index[word]:
        if lastdocid != docid:
            print('document :', docid)
            lastdocid = docid
        wlst = docs[docid]
        line = ' '.join(wlst[max(0, pos-7): pos]).rjust(45) +\
               '\t' + word + '\t' +\
               ' '.join(wlst[pos+1: pos+8]).ljust(45)
        print('\t', line)


##def make_trie(wordlst):
##    counter = Counter()
##    for word in wordlst:
##        counter.update(enumerate(list(word) + [None]))
##    return counter

def make_trie(wordlst):
    trie = {}
    for word in wordlst:
        tmp = trie
        for c in list(word) + [None]:
            if c in tmp:
                tmp[c][1] += 1
            else:
                tmp[c] = [{}, 1]
            tmp = tmp[c][0]
    return trie

def common_suffix(suf):
    sz = len(suf)
    ls = [(w, c) for w, c in all_words.items()
              if w.endswith(suf) and all_words[w[:-sz]] > 10]
    return ls

def common_prefix(pre):
    sz = len(pre)
    ls = [(w, c) for w, c in all_words.items()
              if w.startswith(pre) and all_words[w[sz:]] > 10]
    return ls
